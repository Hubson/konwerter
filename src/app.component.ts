import { Component } from '@angular/core';
import {ExchangeService} from './ExchangeService'

@Component({
  selector: 'converter',
  providers: [ExchangeService],
  template: `<input type="number" [(ngModel)]="kwotaBazowa"
                    [ngClass]="{error:jestFalse(kwotaBazowa), warning:kwotaBazowa<0}">
                    <currency-select [(selected)]='walutaBazowa'></currency-select>
                    = <strong>{{KwotaWyjsciowa | number: '1.2-2'}}</strong>
                    <currency-select [(selected)]='walutaDocelowa'></currency-select>
                    <p *ngIf="jestFalse(kwotaBazowa)">Proszę podać wartość do przeliczenia</p>
                    <p>{{data | date: 'medium'}}</p>
           `,
styles: [`input[type=number] {
        width: 10ex;
        text-align: right;
        }
        .error{
            background-color:#ff5555;
        }
        .warning{
            background-color:#995522;
        }`
        ]
})
export class AppComponent {

    data = Date.now();
    walutaBazowa = 'USD';
    walutaDocelowa = 'GBP';
    kwotaBazowa = 1;

    constructor(private exchangeService: ExchangeService){}

    get KwotaWyjsciowa(){
        const exchangeRate = this.exchangeService.
        getExchangeRate(this.walutaBazowa, this.walutaDocelowa);
        return this.kwotaBazowa * exchangeRate;
    }
    jestFalse(value){
        return !Number.isFinite(value)
    }
}
