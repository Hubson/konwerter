export class ExchangeService{

    supportedCurrencies = ['EUR','GBP','USD'];
    

    private exchangeRates = {
        'USD/GBP':0.70,
        'USD/EUR':1.30,
        'EUR/USD':1.10,
        'EUR/GBP':0.90,
        'GBP/EUR':1.24,
        'GBP/USD':1.42,
    }

    getExchangeRate(walutaBazowa: string, walutaDocelowa: string){
        if(walutaBazowa === walutaDocelowa){
            return 1;
        }
        return this.exchangeRates[walutaBazowa+'/'+walutaDocelowa];
    }
}