System.register(['@angular/core', './ExchangeService'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, ExchangeService_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ExchangeService_1_1) {
                ExchangeService_1 = ExchangeService_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(exchangeService) {
                    this.exchangeService = exchangeService;
                    this.data = Date.now();
                    this.walutaBazowa = 'USD';
                    this.walutaDocelowa = 'GBP';
                    this.kwotaBazowa = 1;
                }
                Object.defineProperty(AppComponent.prototype, "KwotaWyjsciowa", {
                    get: function () {
                        var exchangeRate = this.exchangeService.
                            getExchangeRate(this.walutaBazowa, this.walutaDocelowa);
                        return this.kwotaBazowa * exchangeRate;
                    },
                    enumerable: true,
                    configurable: true
                });
                AppComponent.prototype.jestFalse = function (value) {
                    return !Number.isFinite(value);
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'converter',
                        providers: [ExchangeService_1.ExchangeService],
                        template: "<input type=\"number\" [(ngModel)]=\"kwotaBazowa\"\n                    [ngClass]=\"{error:jestFalse(kwotaBazowa), warning:kwotaBazowa<0}\">\n                    <currency-select [(selected)]='walutaBazowa'></currency-select>\n                    = <strong>{{KwotaWyjsciowa | number: '1.2-2'}}</strong>\n                    <currency-select [(selected)]='walutaDocelowa'></currency-select>\n                    <p *ngIf=\"jestFalse(kwotaBazowa)\">Prosz\u0119 poda\u0107 warto\u015B\u0107 do przeliczenia</p>\n                    <p>{{data | date: 'medium'}}</p>\n           ",
                        styles: ["input[type=number] {\n        width: 10ex;\n        text-align: right;\n        }\n        .error{\n            background-color:#ff5555;\n        }\n        .warning{\n            background-color:#995522;\n        }"
                        ]
                    }), 
                    __metadata('design:paramtypes', [ExchangeService_1.ExchangeService])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
